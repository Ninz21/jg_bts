/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author student
 */
public class JG_MainView {
    JFrame mainFrame = new JFrame();
    JTextField pet_name = new JTextField();
    JButton but = new JButton();

    public JG_MainView(Main controller) {
        mainFrame.add(pet_name);
        mainFrame.add(but);
        this.controller = controller;
        controller.registerPet();
        mainFrame.setVisible(true);
        mainFrame.setSize(350, 300);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
    }
    
    
    Main controller;
    
    
}
